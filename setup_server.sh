#!/bin/bash

# generate key pairs for server

openssl genrsa -out server_rsa 2048
openssl rsa -in server_rsa -pubout > server_pub.pem

